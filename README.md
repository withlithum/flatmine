# FlatMine

FlatMine is a resources pack for Minecraft, which make minecraft textures flat.

## Installation

You can **download the whole repository**, or download stable packages, or even **clone the repository** to the `resourcepacks` folder of your Minecraft client.

If you chooses the third way, the pack is easier to update by using `git pull` to update.